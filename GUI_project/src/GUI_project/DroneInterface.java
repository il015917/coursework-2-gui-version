/**
 * 
 */
package GUI_project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Larang Lingden Limbu
 *
 */
public class DroneInterface extends Application {
	private MyCanvas mc;
	private AnimationTimer timer;								// timer used for animation
	private VBox rtPane;										// vertical box for object's information
	private DroneArena dArena;
	JFileChooser chooser = new JFileChooser(); // to choose a file 

	/**
	 * showAbout
	 * function to show in a box ABout the programme
	 */
	private void showAbout() {
	    Alert alert = new Alert(AlertType.INFORMATION);				// define what box is
	    alert.setTitle("About");									// say is About
	    alert.setHeaderText(null);
	    alert.setContentText("This is a Drone Simulation. \nTo run the program press the RUN button and to pause the program press the PAUSE button. \nTo add drones, advanced drones and obstacles, press the buttons in the TO ADD section.");			// give text
	    alert.showAndWait();										// show box and wait for user to close
	}
	
	/**
	 * saveFile
	 * function to choose where to save a file
	 */
	
	private void saveFile() {
		JFrame parentFrame = new JFrame(); 	//open a new Jfram
		
			chooser.setDialogTitle("Choose file to save");   //outputs to the user
	 
			int userSelection = chooser.showSaveDialog(parentFrame);
	 
			if (userSelection == JFileChooser.APPROVE_OPTION) {
				File fileToSave = chooser.getSelectedFile();
				System.out.println("Save as file: " + fileToSave.getAbsolutePath()); 	//the file path the user wants can be found in the menu
				try {
					PrintWriter file = new PrintWriter(new File(fileToSave.getAbsolutePath()));
					
					file.write(dArena.savingThis());    		//writes into the file			        					    
					file.close(); //closes the file
					System.out.println("BUILDING INFO CORRECTLY SAVED TO FILE");
					
				} catch (FileNotFoundException e) {
			
					System.out.println("ERROR ENCOUNTERED WHILE WRITING TO FILE"); //if the file is invalid
				}
			}
	}

	/**
	 * setMenut
	 * set up the menu of commands for the GUI
	 * @return the menu bar
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create main menu
	
		Menu mFile = new Menu("File");							// add File main menu	
		MenuItem mExit = new MenuItem("Exit");					// whose sub menu has Exit
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
		        System.exit(0);									// exit program
		    }
		});
		
		MenuItem mSave = new MenuItem("Save"); 					//create the save button
		mSave.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				timer.stop();
				saveFile(); 									//calls save file
			}
		});
		
		/*
		MenuItem mLoad = new MenuItem("Load");
		mSave.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				timer.stop();
				loadFile();
			}
		});*/
		
		mFile.getItems().addAll(mSave, mExit);							// add exit and save and load to File menu
		
		
		Menu mHelp = new Menu("Help");							// create Help menu
		MenuItem mAbout = new MenuItem("About");				// add About sub men item
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout();									// and its action to print about
            }	
		});														// add About to Help main item
		mHelp.getItems().addAll(mAbout);	

		menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
		return menuBar;											// return the menu
	}

	/**
	 * setButtons
	 * set up the horizontal box for the bottom with relevant buttons
	 * @return The HBox
	 */
	private HBox setButtons() {
	    Button btnStart = new Button("Start");					// create button for starting
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	// now define event when it is pressed
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// its action is to start the timer
	       }
	    });

	    Button btnStop = new Button("Pause");					// now button for stop
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();									// and its action to stop the timer
	       }
	    });

	    Button btnAdd = new Button("Drone");				// now button for another drone
	    btnAdd.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	dArena.addDrone();								// and its action to add drone
	           	drawWorld();
	       }
	    });
	    
	    Button btnAddObstacle = new Button("Obstacle");				// now button for another obstacle
	    btnAddObstacle.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	dArena.addObstacle();								// and its action to add obstacle
	           	drawWorld();
	       }
	    });
	    
	    Button btnAddAdvancedDrone = new Button("Advanced Drone");				// now button for another sensor drone
	    btnAddAdvancedDrone.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	dArena.addAdvancedDrone();								// and its action to add another sensor drone
	           	drawWorld();
	       }
	    });
	    														// now add these buttons + labels to a HBox
	    return new HBox(new Label("To Run: "), btnStart, btnStop, new Label("To Add: "), btnAdd, btnAddAdvancedDrone, btnAddObstacle);
	}

	/** 
	 * drawWorld
	 * draw the world with ball in it
	 */
	public void drawWorld () {
	 	mc.clearCanvas();						// set beige colour
	 	dArena.drawArena(mc);
	}
	
	/**
	 * drawStatus
	 * show where ball is, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();					// clear rtpane
		ArrayList<String> allBs = dArena.describeAll();
		for (String s : allBs) {
			Label l = new Label(s); 		// turn description into a label
			rtPane.getChildren().add(l);	// add label	
		}	
	}



	/**
	 *
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Drone Simulation");
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));

	    bp.setTop(setMenu());											// put menu at the top

	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas( 1000, 700 );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// load canvas to left area
	
	    mc = new MyCanvas(canvas.getGraphicsContext2D(), 1000, 700);
	    
	    StackPane holder = new StackPane();
	    holder.getChildren().add(canvas);
	    root.getChildren().add(holder);
	    holder.setStyle("-fx-background-color: lightgrey");  			//change background colour

	    dArena = new DroneArena(1000, 700);								// set up arena
	    drawWorld();
	    
	    timer = new AnimationTimer() {									// set up timer
	        public void handle(long currentNanoTime) {					// and its action when on
	        		dArena.checkObjects();									// check the angle of all object
	        		dArena.adjustObjects();								// move all moveable objects
		            drawWorld();										// redraw the world
		            drawStatus();										// indicate where the objects are
	        }
	    };

	    rtPane = new VBox();											// set vBox on right to list items
		rtPane.setAlignment(Pos.TOP_LEFT);								// set alignment
		rtPane.setPadding(new Insets(5, 75, 75, 5));					// padding
 		bp.setRight(rtPane);											// add rtPane to borderpane right
		  
	    bp.setBottom(setButtons());										// set bottom pane with buttons

	    Scene scene = new Scene(bp, 1200, 1000);							// set overall scene
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	  

	}

	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
	    Application.launch(args);			// launch the GUI

	}

}