/**
 * 
 */
package GUI_project;

import javafx.scene.image.Image;

/**
 * @author Larang Lingden Limbu
 *
 */
public class AdvancedDrone extends Object {
	
	
	double advDroneAngle, advDroneSpeed;			// angle and speed of travel
	Image drone; 									//the image of the drone
	
	/** 
	 * AdvancedDrone
	 * Create advanced drone, size ir at ix,iy, moving at angle ia and speed is
	 * @param ix the x position of the advanced drone
	 * @param iy the y position of the advanced drone
	 * @param ir the radius/size of the advanced drone
	 * @param ia the angle of the movement of the advanced drone
	 * @param is the speed of movement of the advanced drone
	 */
	public AdvancedDrone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		advDroneAngle = ia;
		advDroneSpeed = is;
		rad = ir;
		drone = new Image(getClass().getResourceAsStream("advanceddrone.png")); //gets the image for this object
	}
	
	/**
	 * drawObject
	 * calls drawDrone
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawAdvDrone(mc);
	}
	
	/**
	 * @param mc canvas - put the drone in the canvas
	 */
	public void drawAdvDrone(MyCanvas mc) {
		mc.drawIt(drone, x/mc.getCanvasSizeX(), y/mc.getCanvasSizeY(), rad); //draws the image in the position and size
	}

	/**
	 * checkdrone - change angle of travel if hitting wall or another object
	 * @param b   droneArena
	 */
	@Override
	protected void checkObject(DroneArena b) {;
		advDroneAngle = b.CheckDroneAngle(x, y, rad*2, advDroneAngle, ObjectID);
	}

	/**
	 * adjustdrone
	 * Here, move drone depending+ on speed and angle
	 */
	@Override
	protected void adjustObject() {
		double radAngle = advDroneAngle*Math.PI/180;		// put angle in radians
		x += advDroneSpeed * Math.cos(radAngle);			// new X position
		y += advDroneSpeed * Math.sin(radAngle);			// new Y position
	}
	
	/**
	 * saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return (" Advanced Drone at: " + x + " , "+ y + " Angle " + advDroneAngle + " Speed " + advDroneSpeed);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Advanced Drone ";
	}
	
}