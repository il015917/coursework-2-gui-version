/**
 * 
 */
package GUI_project;

import java.util.ArrayList;

/**
 * @author Larang Lingden Limbu
 *S
 */
public class DroneArena {	
	
	double sizeX, sizeY, randX, randY, randomAngle, randomSpeed;					// size of arena, random angle, position and speed of the objects
	private ArrayList<Object> allObjects;												// array list of all Objects in arena
	/**
	 * construct an arena
	 */
	DroneArena() {
		this(1000, 700);			// default size
	}
	/**
	 * construct arena of size xS by yS
	 * @param xS gets the X size of the arena
	 * @param yS gets the Y size of the arena
	 */
	DroneArena(double xS, double yS){
		sizeX = xS;
		sizeY = yS;
		allObjects = new ArrayList<Object>();					// list of all objects, initially empty
	}
	
	/**
	 * getRandomDoubleBetweenRange
	 * @param min minimum value
	 * @param max max value
	 * @return returns a random double between the minimum value and maximum value
	 */
	public static double getRandomDoubleBetweenRange(double min, double max) {
		double x = (Math.random()*((max-min)+1))+min;
		return x;
	}
	/**
	 * getXSize
	 * return arena size in x direction
	 * @return the x size
	 */
	public double getXSize() {
		return sizeX;
	}
	/**
	 * getYSize
	 * return arena size in y direction
	 * @return the y size
	 */
	public double getYSize() {
		return sizeY;
	}
	/**
	 * drawArena
	 * @param mc - draw all objects into the canvas mc
	 */
	public void drawArena(MyCanvas mc) {
		for (Object b : allObjects) b.drawObject(mc);		// draw all objects
	}
	/**
	 * checkObjects
	 * check all objects .. see if need to change angle of moving drones, etc 
	 */
	public void checkObjects() {
		for (Object b : allObjects) b.checkObject(this);	// check all balls
	}
	/**
	 * adjustObjects
	 * adjust all objects .. move any moving ones
	 */
	public void adjustObjects() {
		for (Object b : allObjects) b.adjustObject();
	}
	/**
	 * describeAll
	 * return list of strings defining each object
	 * @return answer
	 */
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
		for (Object b : allObjects) ans.add(b.toString());			// add string defining each object
		return ans;												// return string list
	}
	
	/**
	 * savingThis
	 * returns a string to be saved with all the information of all objects currently in the arena
	 * @return string to save
	 */
	public String savingThis() {
		
		String toSave = "";
		
		for (Object b: allObjects) {
			toSave += b.saving() + "\n";
		}
		
		return toSave;
	}
	
	/*public String loadingThis() {
		String toLoad = "";
	}*/
	
	/** 
	 * checkDroneAngle
	 * Check angle of drone ... if hitting wall, rebound; if hitting other drones, change angle
	 * @param x				drone x position
	 * @param y				drone y position
	 * @param rad			radius
	 * @param ang			current angle
	 * @param notID			identity of object not to be checked
	 * @return				new angle 
	 */
	public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		if (x < rad || x > sizeX - rad) ans = 180 - ans;
			// if drone hit (tried to go through) left or right walls, set mirror angle, being 180-angle
		if (y < rad || y > sizeY - rad) ans = - ans;
			// if try to go off top or bottom, set mirror angle
		
		for (Object b : allObjects) 
			if (b.getID() != notID && b.hitting(x, y, rad*550)) ans = 180*Math.atan2(y-b.getY(), x-b.getX())/Math.PI;
				// check all drone except one with given id
				// if hitting, return angle between the other drone and this one.
		
		return ans;		// return the angle
	}

	/**
	 * checkHit
	 * check if the target object has been hit by another object
	 * @param target	the target object
	 * @return 	true if hit
	 */
	public boolean checkHit(Object target) {
		boolean ans = false;
		for (Object b : allObjects)
			if (b instanceof Drone && b.hitting(target)) ans = true;
				// try all drone if hitting the target
		return ans;
	}
	
	/**
	 * addObstacle
	 * adds obstacle in a random position
	 */
	public void addObstacle() {
		randX = getRandomDoubleBetweenRange(0, getXSize());
		randY = getRandomDoubleBetweenRange(0, getYSize());
		
		allObjects.add(new Obstacle(randX, randY, 0.09));
	}
	
	/**
	 * addDrone
	 * adds drone in random position with random angle and speed
	 */
	public void addDrone() {
		randX = getRandomDoubleBetweenRange(0, getXSize());
		randY = getRandomDoubleBetweenRange(0, getYSize());
		randomAngle = getRandomDoubleBetweenRange(0,360);
		randomSpeed = getRandomDoubleBetweenRange(1,3);
		allObjects.add(new Drone(randX, randY, 0.1, randomAngle, randomSpeed));
	}
	
	/**
	 * addAdvancedDrone
	 * adds advanced drone in random position with random angle and speed
	 */
	public void addAdvancedDrone() {
		randX = getRandomDoubleBetweenRange(0, getXSize());
		randY = getRandomDoubleBetweenRange(0, getYSize());
		randomAngle = getRandomDoubleBetweenRange(0,360);
		randomSpeed = getRandomDoubleBetweenRange(1,2);
		allObjects.add(new AdvancedDrone(randX, randY, 0.1, randomAngle, randomSpeed));
	}
}