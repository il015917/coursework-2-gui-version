/**
 * 
 */
package GUI_project;

/**
 * @author Larang Lingden Limbu
 *
 */
import javafx.scene.image.Image;

public class Drone extends Object {

	double droneAngle, droneSpeed;			// angle and speed of travel

	Image drone; 							//image of the drone
	
	/** 
	 * Drone
	 * Create drone, size ir at ix,iy, moving at angle ia and speed is
	 * @param ix the x position of the drone
	 * @param iy the y position of the drone
	 * @param ir the radius/size of the drone
	 * @param ia the angle of the movement of the drone
	 * @param is the speed of movement of the drone
	 */
	public Drone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		droneAngle = ia;
		droneSpeed = is;

		rad = ir;
		drone = new Image(getClass().getResourceAsStream("drone.png")); //the image of the drone
	}
	
	/**
	 * drawObject
	 *calls drawDrone
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawDrone(mc);
	}
	
	/**
	 * drawDrone
	 * @param mc canvas
	 */
	public void drawDrone(MyCanvas mc) {
		mc.drawIt(drone, x/mc.getCanvasSizeX(), y/mc.getCanvasSizeY(), rad); //draws the drone into the canvas in the size and position
	}

	/**
	 * checkObject 
	 * @param d droneArena
	 */
	@Override
	protected void checkObject(DroneArena d) {;
		droneAngle = d.CheckDroneAngle(x, y, rad, droneAngle, ObjectID); //change the angle of the drone
	}

	/**
	 * adjustObject
	 * Here, move drone depending on speed and angle
	 */
	@Override
	protected void adjustObject() {
		double radAngle = droneAngle*Math.PI/180;		// put angle in radians
		x += droneSpeed * Math.cos(radAngle);		// new X position
		y += droneSpeed * Math.sin(radAngle);		// new Y position
	}
	
	/**
	* saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return ("Drone at: " + x + " , "+ y + " Angle " + droneAngle + " Speed " + droneSpeed);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Drone ";
	}
	
}
