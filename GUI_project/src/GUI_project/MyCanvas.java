/**
 * 
 */
package GUI_project;

/**
 * @author Larang Lingden Limbu
 *
 */
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class MyCanvas {
	int Canvas_x = 1000;				// constants for relevant sizes
	int Canvas_y = 700;
    GraphicsContext gc; 

    /**
     * MyCanvas
     * constructor sets up relevant Graphics context and size of canvas
     * @param g graphics context
     * @param xc the x canvas size
     * @param yc the y canvas size
     */
    public MyCanvas(GraphicsContext g, int xc, int yc) {
    	gc = g;
    	Canvas_x = xc;
    	Canvas_y = yc;
    }
    /**
     * getXCanvasSize
     * get size in x of canvas
     * @return xsize
     */
    public int getCanvasSizeX() {
    	return Canvas_x;
    }
    /**
     * getYCanvasSize
     * get size of xcanvas in y    
     * @return ysize
     */
    public int getCanvasSizeY() {
    	return Canvas_y;
    }

    /**
     * clearCanvas
     * clear the canvas
     */
    public void clearCanvas() {
		gc.clearRect(0,  0,  Canvas_x,  Canvas_y);		// clear canvas
    }
    
	/**
	 * drawIt
     * drawIt ... draws object defined by given image at position and size
     * @param i		image
     * @param x		xposition	in range 0..1
     * @param y     y position
     * @param sz	size
     */
	public void drawIt (Image i, double x, double y, double sz) {
			// to draw centred at x,y, give top left position and x,y size
			// sizes/position in range 0..1, so scale to canvas size 
		gc.drawImage(i, Canvas_x * (x - sz/2), Canvas_y*(y - sz/2), Canvas_x*sz, Canvas_y*sz);
	}

}