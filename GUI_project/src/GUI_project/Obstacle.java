/**
 * 
 */
package GUI_project;

import javafx.scene.image.Image;

/**
 * @author Larang Lingden Limbu
 *
 */
public class Obstacle extends Object {
	

	Image obstacle; //image of the obstacle
	
	public Obstacle() {	
	}

	/**
	 * Obstacle
	 * @param ix x position of the obstacle
	 * @param iy y position of the obstacle
	 * @param ir radius/size of the obstacle
	 */
	public Obstacle(double ix, double iy, double ir) {
		super(ix, iy, ir);


		obstacle = new Image(getClass().getResourceAsStream("obstacle.png")); //gets the image of the obstacle
	}	
	
	/**
	 * drawObject
	 * calls drawObstacle
	 */
	@Override
	public void drawObject(MyCanvas mc) {
		drawObstacle(mc);
	}
	
	/**
	 * drawObstacle
	 * @param mc canvas
	 */
	public void drawObstacle(MyCanvas mc) {
		mc.drawIt(obstacle, x/mc.getCanvasSizeX(), y/mc.getCanvasSizeY(), rad); //draws the obstacle in the x and y position with size
	}

	
	/**
	 * checkObject
	 */
	@Override
	protected void checkObject(DroneArena b) {}
	
	/**
	 * adjustObject
	 */
	@Override
	protected void adjustObject() {}
	
	/**
	 * saving
	 * string that will store data that will be saved
	 */
	@Override
	public String saving() {
		return ("Obstacle at: " + x + " , "+ y);
	}
	
	/**
	 * getStrType
	 * return string defining drone type
	 */
	protected String getStrType() {
		return "Tree ";
	}	

}
